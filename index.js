function addition(addend1, addend2) {

	console.log("Displayed sum of " + addend1 + " and " + addend2);
	console.log(addend1 + addend2)
}

addition(5, 15);

function subtraction(minuend, subtrahend) {

	console.log("Displayed difference of " + minuend + " and " + subtrahend);
	console.log(minuend - subtrahend);

}

subtraction(20, 5);

function multiplication(multiplicand, multiplier) {

	return multiplicand * multiplier

}

let product = multiplication(8,5);
console.log("Displayed product of 8 and 5:");
console.log(product);

function division(dividend, divisor) {

	return dividend / divisor

}

let quotient = division(40, 5);
console.log("Displayed quotient of 40 and 5:");
console.log(quotient);

function radiusArea(area, radius) {

	return area * radius ** 2;

}

let circleArea = radiusArea(3.1416, 15);
console.log("The result of getting the area of a circle with 15 radius:")
console.log(circleArea);

function average(addend1, addend2, addend3, addend4, divisor) {

	return (addend1 + addend2 + addend3 + addend4) / divisor

}

let averageVar = average(20, 40, 60, 80, 4);
console.log("The average of 20, 40, 60, 80:");
console.log(averageVar);

function grade(score, totalItems) {

	return (score / totalItems) * 100;
}
let isPassed = grade(38, 50);
let isPassingScore = (isPassed >= 75);
console.log("Is 38/50 a passing grade?");
console.log(isPassingScore);